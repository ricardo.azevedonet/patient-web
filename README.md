# PatientWeb

Este projeto foi gerado com [Angular CLI](https://github.com/angular/angular-cli) version 10.0.1.

## Para baixar as dependências dor projeto executar o comando

```shell
npm install
```


## Execução da aplicação localmente

```shell
ng serve --proxy-config proxy.conf.json
```

