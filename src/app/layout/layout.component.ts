import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { ToastService } from '../core/services/toast.service';
import { LoaderService } from '../core/services/loader.service';
import { MenuDataService } from '../core/services/menu-data.service';
import { CustomMenuItem } from '../core/models/menu-item.model';
import { User } from '../core/models/user.model';
import { Sidebar } from 'primeng/sidebar';
import { ApplicationStateService } from '../core/services/application-state.service';
import { SessionService } from '../core/services/session.service';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.css']
})
export class LayoutComponent implements AfterViewInit, OnInit {

  menuItems: CustomMenuItem[] = new Array();

  isMenuVisible: boolean;

  isMobileResolution = false;

  @ViewChild('menubar') menubar: Sidebar;

  constructor(
    private toastService: ToastService,
    private loaderService: LoaderService,
    private menuDataService: MenuDataService,
    private applicationStateService: ApplicationStateService,
    private sessionService: SessionService
    ) {
  }

  ngOnInit() {
    this.loaderService.display(true);
    this.toastService.addSingle('success', '', 'Login successfully.');

    this.menuDataService.getMenuList().forEach(e => {
      const menuItem = e as CustomMenuItem;
      if (menuItem.role.length > 0 && User.verifyContainsRole(this.sessionService.getItem('currentUser'), 'ROLE_' + menuItem.role)) {
        this.menuItems.push(menuItem);
      }
    });

    this.isMobileResolution = this.applicationStateService.getIsMobileResolution() as boolean;
    if (this.isMobileResolution) {
      this.isMenuVisible = false;
    } else {
      this.isMenuVisible = true;
    }
  }

  ngAfterViewInit(): void {
    setTimeout(() => {
      this.loaderService.display(false);
    }, 1000);
  }

  toggleMenu() {
    this.isMenuVisible = !this.isMenuVisible as boolean;
  }

}
