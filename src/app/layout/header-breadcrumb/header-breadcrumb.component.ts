import { Component, OnInit } from '@angular/core';
import { MenuItem } from 'primeng/api';
import { RouteStateService } from '../../core/services/route-state.service';
import { MytranslatorService } from '../../core/services/mytranslator.service';

@Component({
  selector: 'app-header-breadcrumb',
  templateUrl: './header-breadcrumb.component.html',
  styleUrls: ['./header-breadcrumb.component.css']
})
export class HeaderBreadcrumbComponent implements OnInit {

  items: MenuItem[];

  home: MenuItem;

  constructor(private routeStateService: RouteStateService, private mytranslatorService: MytranslatorService) {
    this.items = [];
  }

  ngOnInit() {
    var routes = this.routeStateService.getAll();

    routes.forEach(route => {
      this.items.push({ label: this.mytranslatorService.getTranslator(route.title), command: () => { this.onClickBreadcrumb(route.id); } });
    });

    this.home = {icon: 'pi pi-home', routerLink: ['/home']};
  }

  onClickBreadcrumb(id: number) {
    this.routeStateService.loadById(id);
  }
}
