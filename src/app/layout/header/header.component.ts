import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { RouteStateService } from '../../core/services/route-state.service';
import { SessionService } from '../../core/services/session.service';
import { User } from '../../core/models/user.model';
import { notification } from '../../core/models/notification.model';
import { UserIdleService } from 'angular-user-idle';
import { ThemeService } from '../../core/services/theme.service';
import { SocketService } from '../../core/services/socket.service';
import { ApplicationStateService } from '../../core/services/application-state.service';

import { TranslateService } from '@ngx-translate/core';
import { DashboardComponent } from '../../dashboard/dashboard.component';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
  providers: [DashboardComponent]
})
export class HeaderComponent implements OnInit {

  user: User;

  displayNotifications: boolean;

  notifications: notification[];

  @Output() toggleMenubar: EventEmitter<any> = new EventEmitter();

  private _translate: TranslateService

  constructor(

    private router: Router,
    private routeStateService: RouteStateService,
    private sessionService: SessionService,
    private userIdle: UserIdleService,
    private themeService: ThemeService,
    private t: TranslateService,
    private socketService: SocketService,
    private dashboardComponent: DashboardComponent) {

    t.setDefaultLang('pt_BR');

    this.displayNotifications = false;

    const selectedTheme = this.sessionService.getItem('selected-theme');
    if (selectedTheme) {
      this.selectTheme(selectedTheme);
    }
    this._translate = t;
  }

  ngOnInit() {
    this.user = this.sessionService.getItem('currentUser');
    this.notifications = [];
    /*for (var i = 1; i <= 5; i++) {
      var notificationObj = new notification("Message " + i, new Date(), null)
      this.notifications.push(notificationObj);
    }*/

    this.socketService.connect(this.notifications,  this.user);

    this.userIdle.startWatching();

    // Start watching when user idle is starting.
    this.userIdle.onTimerStart().subscribe();

    // Start watch when time is up.
    this.userIdle.onTimeout().subscribe(() => {
      this.logout();
    });
  }

  logout() {
    this.userIdle.stopWatching();
    this.routeStateService.removeAll();
    this.sessionService.removeItem('currentUser');
    this.sessionService.removeItem('active-menu');
    this.router.navigate(['/login']);
  }

  showNotificationSidebar() {
    this.displayNotifications = true;
  }

  toggleMenu() {
    this.toggleMenubar.emit();
  }

  selectTheme(theme: string) {
    this.sessionService.setItem('selected-theme', theme);
    this.themeService.selectTheme(theme);
  }


  useLanguage(lang) {
    this._translate.use(lang);
  }

  getSizeNotifications() {
    return this.notifications.length;
  }

  get translate(){
    return this._translate;
  }

  refresh(lang){
    this.useLanguage(lang)
    this.dashboardComponent.startService();
  }

}
