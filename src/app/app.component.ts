import { Component, OnInit } from '@angular/core';
import { LoaderService } from './core/services/loader.service';
import { ThemeService } from './core/services/theme.service';

import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'Ng-Prime';
  showLoader: boolean;
  theme: string;
  private _translate: TranslateService

  constructor(private loaderService: LoaderService,
              private themeService: ThemeService,
              private t: TranslateService) {
    t.setDefaultLang('pt_BR');
    this._translate = t;
    this.theme = "dark-theme";
  }

  ngOnInit() {
    this.loaderService.status.subscribe((val: boolean) => {
      this.showLoader = val;
    });

    this.themeService.theme.subscribe((val: string) => {
      this.theme = val;
    });
  }

  useLanguage(lang) {
    this._translate.use(lang);
  }

  get translate(){
    return this._translate;
  }
}
