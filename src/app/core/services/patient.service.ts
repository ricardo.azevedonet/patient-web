import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Patient } from '../models/patient.model';
import { Observable } from 'rxjs';
import { GenericFilter } from '../models/generic/generic-filter.model';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};


@Injectable({
  providedIn: 'root'
})
export class PatientService {

  private autoCompleteUrl = '/patient-api/patient/v1/search/';
  paginationUrl = '/patient-api/patient/v1/pagination?';
  changeStatusrUrl = '/patient-api/patient/v1/change_status/';
  registerUrl = '/patient-api/patient/v1/register/';
  updaterUrl = '/patient-api/patient/v1/updatePatient';


  constructor(private http: HttpClient) { }

  autoComplete(name: string): Observable<Patient[]> {
    return this.http.get<Patient[]>(this.autoCompleteUrl + name, httpOptions);
  }

  findPaginator(filter: GenericFilter, page: number, size: number): Observable<any[]> {
    return this.http.put<any[]>(this.paginationUrl + 'page=' + page + '&size=' + size, filter, httpOptions);
  }

  changeStatus(patient: Patient): Observable<any[]> {
    return this.http.post<any[]>( this.changeStatusrUrl , patient, httpOptions);
  }

  addOrUpdate(patient: Patient): Observable<any[]> {

    if ( patient.id == null ){
       return this.http.post<any[]>(this.registerUrl, patient, httpOptions);
    }

    return this.http.put<any[]>(`${this.updaterUrl}/${patient.id}`, patient, httpOptions);

  }

}
