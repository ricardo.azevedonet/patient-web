import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Address } from '../models/address.model';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};


@Injectable({
  providedIn: 'root'
})
export class AddresszipcodeService {
  addresszipcodeUrl = '/patient-api/endereco/v1/';

  constructor(private http: HttpClient) { }

  findZipcode(zipCode: string): Observable<Address[]> {
    return this.http.get<Address[]>(this.addresszipcodeUrl + zipCode, httpOptions);
  }

}