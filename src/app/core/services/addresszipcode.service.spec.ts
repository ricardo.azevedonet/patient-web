import { TestBed } from '@angular/core/testing';

import { AddresszipcodeService } from './addresszipcode.service';

describe('AddresszipcodeService', () => {
  let service: AddresszipcodeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AddresszipcodeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
