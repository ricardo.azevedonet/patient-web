import { Injectable } from '@angular/core';

import * as Stomp from '@stomp/stompjs';
import * as Webstomp from '@stomp/stompjs';
import { notification } from '../../core/models/notification.model';
import * as SockJS from 'sockjs-client';

import { User } from '../models/user.model';

@Injectable({
  providedIn: 'root'
})
export class SocketService {
  notificationsTMP: notification[] = [];
  disabled = true;
  name: string;
  private stompClient = null;

  constructor() { }

  setConnected(connected: boolean) {
    this.disabled = !connected;
  }
  //http://localhost:4200/api/websocketApp
//'http://localhost:8025/loterias/websocketApp
  connect( notifications: notification[], user: User) {
   /* const socket = new SockJS('http://localhost:8025/loterias/websocketApp');
    this.stompClient = Webstomp.Stomp.over(socket);
    this.notificationsTMP = [];
    const _this = this;
    this.stompClient.connect({}, function(frame) {
      _this.setConnected(true);

      _this.stompClient.subscribe( '/topic/notification', function(message) {
        const n = JSON.parse(message.body) as notification;
        if (n.userId === user.userId) {
          notifications.push(n);
          notifications.sort((val1, val2) => {
          const val2CreatedOn = new Date(val2.createdOn) as Date;
          const val1CreatedOn = new  Date(val1.createdOn) as Date;

          return val2CreatedOn.getTime() - val1CreatedOn.getTime();
          });
        }
      });
    });*/
  }

  disconnect() {
    if (this.stompClient != null) {
      this.stompClient.disconnect();
    }

    this.setConnected(false);
    console.log('Disconnected!');
  }

  sendName() {
    this.stompClient.send(
      '/app/chat.sendMessage',
      {},
      JSON.stringify({"name": this.name })
    );
  }


}
