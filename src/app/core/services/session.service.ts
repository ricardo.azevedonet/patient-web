import { Injectable } from '@angular/core';


const TOKEN_KEY = 'AuthToken';

@Injectable()
/**
 * Session storage service
 * Provides methods to get, set, remove, clear session storage items.
 */
export class SessionService {
    /**
     * set session storage item
     * @param key 
     * @param value 
     */
    setItem(key: string, value: any) {
        sessionStorage.setItem(key, JSON.stringify(value));
    }

    /**
     * get session storage item
     * @param key 
     */
    getItem(key: string): any {
        var value = sessionStorage.getItem(key);
        return JSON.parse(value);
    }

    /**
     * remove session storage item
     * @param key
     */
    removeItem(key: string) {
        sessionStorage.removeItem(key);
    }

    /**
     * remove all session storage items
     */
    clear() {
        sessionStorage.clear();
    }

    public saveToken(token: string) {
        sessionStorage.removeItem(TOKEN_KEY);
        sessionStorage.setItem(TOKEN_KEY, token);
    }

    public getToken(): string {
        var value = sessionStorage.getItem(TOKEN_KEY);
        return value;
    }

}