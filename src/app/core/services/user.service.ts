import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams, HttpResponse} from '@angular/common/http';
import { Observable } from 'rxjs';

import { AuthLoginInfo } from '../models/login-info';
import { User } from '../models/user.model';
import { JwtResponse } from '../auth/jwt-response';
import { Page } from '../models/page.model';

import { GenericFilter } from '../../core/models/generic/generic-filter.model';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class UserService {

private loginUrl =  '/patient-api/user/v1/';
private registerUserUrl =  '/patient-api/user/v1/signup/';
private updateUserUrl  =  '/patient-api/user/v1/updateUser';
private changeStatusUserUrl  =  '/patient-api/user/v1/change_status_user/';
private autoCompleteUrl =  '/patient-api/user/v1/search/';
private lazyUserUrl =  '/entity/pagination/';
private userPaginationUrl = '/patient-api/user/v1/pagination?';

constructor(private http: HttpClient) {}

  attemptAuth(credentials: AuthLoginInfo ): Observable<JwtResponse> {
    return this.http.post<JwtResponse>(this.loginUrl, credentials, httpOptions);
  }

  autoCompleteUsers(name: string): Observable<JwtResponse[]> {
    return this.http.get<JwtResponse[]>(this.autoCompleteUrl + name, httpOptions);
  }

  getUsers() {
    return this.http.get<any>(this.lazyUserUrl)
    .toPromise()
    .then(res => < User[] > res.data )
    .then(data => { return data; } );
  }

  queryPaginated<T>(baseUrl: string, urlOrFilter?: string | object): Observable<Page<T>> {
    let params = new HttpParams();
    let url = baseUrl;

    if (typeof urlOrFilter === 'string') {
      // we were given a page URL, use it
      url = urlOrFilter;
    } else if (typeof urlOrFilter === 'object') {
      // we were given filtering criteria, build the query string
      Object.keys(urlOrFilter).sort().forEach(key => {
        const value = urlOrFilter[key];
        if (value !== null) {
          params = params.set(key, value.toString());
        }
      });
    }

    return this.http.get<Page<T>>(url, {params: params});
  }

  findUserPaginator(filer: GenericFilter, page: number, size: number): Observable<any[]> {
    return this.http.put<any[]>(this.userPaginationUrl + 'page=' + page + '&size=' + size, filer, httpOptions);
  }

  addOrUpdateUser(user: User): Observable<any[]> {

    if ( user.id == null ){
       return this.http.post<any[]>(this.registerUserUrl, user, httpOptions);
    }

    return this.http.put<any[]>(`${this.updateUserUrl}/${user.id}`, user, httpOptions);
  }

  changeStatusUser(user: User): Observable<any[]> {
    return this.http.post<any[]>( this.changeStatusUserUrl , user, httpOptions);
  }
}
