import { Injectable } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';

@Injectable({
  providedIn: 'root'
})
export class MytranslatorService {

  constructor(private translate: TranslateService) { }

  getTranslator(key: string): string {
    let ret = '';
    this.translate.get(key).subscribe((value: string) => {
      ret = value;
    });
    return  ret;
  }

  getTranslatorarametres(key: string, parametres: string[]): string {
    let ret = '';
    this.translate.get(key, parametres).subscribe((value: string) => {
      ret = value;
    });
    return  ret;
  }
}
