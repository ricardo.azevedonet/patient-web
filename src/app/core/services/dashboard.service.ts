import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Dashboard } from '../models/dashboard.model';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};


@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  search = '/patient-api/dashboard/v1/search/';

  constructor(private http: HttpClient) { }

  searchDashboard(): Observable<Dashboard> {
    return this.http.get<Dashboard>(this.search, httpOptions);
  }

}
