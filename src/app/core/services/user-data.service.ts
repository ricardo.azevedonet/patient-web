import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';
import { User } from '../models/user.model';
import { AuthLoginInfo } from '../models/login-info';


import { SessionService } from '../services/session.service';

import { GenericFilter } from '../../core/models/generic/generic-filter.model';
import { Role } from '../models/role.model';
import { UserService } from './user.service';

@Injectable()
/**
 * user service class
 */
export class UserDataService {

    users: User[] = [];
    private user: User;
    constructor(private userService: UserService, private sessionService: SessionService) {}

    getUserByUserNameAndPassword(username: string, password: string): Observable<User>  {

        const loginInfo = new AuthLoginInfo(username, password);
        this.user = new User();
        this.userService.attemptAuth(loginInfo).subscribe(
            data => {
                console.log('deu data := ' + JSON.stringify(data));

                this.user.id = Number(data.id);
                this.user.token = data.token;
                this.user.username = data.username;
                this.user.email = data.username;
                console.log('deu user final := ' + this.user);
                return this.user;
            },
            error => {
              console.log('deu pau error := ' + error);
            }
        );
        console.log('veja o user null ');
        return null;
    }

    addUser(name: string, username: string, email: string): boolean {
        const user = new User();
        user.name = name;
        user.username = username;
        user.email = email;
        this.users.push(user);
        return true;
    }

    addOrUpdateUser(id: number, name: string, username: string, email: string, userSelected: User): Observable<any[]>  {

      const currentUser = this.sessionService.getItem('currentUser');

      const user = userSelected === null ? new User() : userSelected;
      user.id = id;
      user.name = name;
      user.username = username;
      user.email = email;
      user.userChangeId = currentUser.id;
      if ( !user.role){
        user.role.push('user');
      }

      return this.userService.addOrUpdateUser(user);
    }

    autoCompleteNameUser(username: string): Observable<any> {
      return  this.userService.autoCompleteUsers(username);
    }

    getUsers() {
      return  this.userService.getUsers();
    }

    findUserPaginator(filter: GenericFilter, page: number, size: number): Observable<any[]> {
      return  this.userService.findUserPaginator(filter, page, size);
    }

    changeStatusUser(user: User): Observable<any[]>  {
      const userTMP = this.sessionService.getItem('currentUser');
      user.userChangeId = userTMP.id;
      return this.userService.changeStatusUser(user);
    }

}
