import { Injectable } from '@angular/core';

@Injectable()
/**
 * menu data service
 */
export class MenuDataService {
    getMenuList() {
        return [
           {
              Label: 'bundle.label.dashboard', Icon: 'fa fa-dashboard',
                RouterLink: '/home/dashboard', Childs: null, IsChildVisible: false,
              role: 'USER'
            },
            {
              Label: 'bundle.label.list.patient', Icon: 'fa fa-universal-access',
                RouterLink: '/home/list-patient', Childs: null, IsChildVisible: false,
              role: 'USER'
            },
            {
              Label: 'bundle.label.list.users', Icon: 'fa fa-users',
                RouterLink: '/home/list-user', Childs: null, IsChildVisible: false,
              role: 'ADMIN'
            }
        ];
    }
}
