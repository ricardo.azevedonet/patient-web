import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { SessionService } from '../services/session.service';

import { User } from '../../core/models/user.model';

@Injectable()
export class AuthGuard implements CanActivate {

    user: User;
    constructor(private router: Router, private sessionService: SessionService) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        this.user = this.sessionService.getItem('currentUser');
        if (this.user) {
          if (route.data.roles && this.user.role.includes(route.data.roles)) {
            // role not authorised so redirect to home page
            this.router.navigate(['/']);
            return false;
          }
            // logged in so return true
            return true;
        }

        // not logged in so redirect to login page with the return url and return false
        this.router.navigate(['/login'], { queryParams: { returnUrl: state.url } });
        return false;
    }
}
