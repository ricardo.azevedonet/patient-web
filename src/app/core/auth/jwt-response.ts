import { Role } from '../models/role.model';

export class JwtResponse {
    id: string;
    token: string;
    type: string;
    username: string;
    email: string;
    roles: [];
}
