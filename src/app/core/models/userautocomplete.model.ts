export class UserAutocomplete {

    id: number;
    username: string;
    email: string;
    authorities: string[];

}
