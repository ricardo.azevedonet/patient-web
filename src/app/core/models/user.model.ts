import { Role } from '../models/role.model';

export class User {

    id: number;
    username: string;
    name: string;
    email: string;
    token: string;
    role: string[] = [];
    status: string;
    userChangeId: number;
    userChangeName: string;
    dateChange: Date;

    static verifyContainsRole(user: User, nameRole: string) {
      let ret = false;
      user.role.forEach(r => {
        const authority = r as string;
        if (authority === nameRole) {
          ret = true;
        }
      });

      return ret;
    }

}
