import { User } from './user.model';

export class Patient {
    id: number;
    name: string;
    cpf: string;
    email: string;
    userChange: User;
    status: string;
    dateChange: Date;
    address: string;
    number: string;
    complement: string;
    district: string;
    city: string;
    state: string;
    zipCode: string;
    telefone: string;
    dataBirth: Date;
}
