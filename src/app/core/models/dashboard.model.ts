import { User } from './user.model';

export class Dashboard {

    sumActive: number;
    sumInaActive: number;
    countUsers: number;
    countPatients: number;
    usersCountActive: number;
    usersCountInaActive: number;
    patientsCountActive: number;
    patientsCountInaActive: number;
}
