export class GenericFilter {
  constructor() {
      this.id = null;
      this.type = null;
      this.status = null;
      this.name = null;
  }

    id: string;
    type: string;
    status: string;
    name: string;
}
