// angular default
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
// import ngx-translate and the http loader
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import { TranslateService } from '@ngx-translate/core'

// Prime NG
import { MessageService } from 'primeng/api';
import { NgPrimeModule } from './app.ngprime.module';
// app related
import { AppComponent } from './app.component';
import { AuthGuard } from './core/gaurds/auth.gaurd';
import { AppRoutingModule } from './app.routing.module';
import { LoginComponent } from './login/login.component';
import { LayoutComponent } from './layout/layout.component';
import { MenuComponent } from './layout/menu/menu.component';
import { HeaderComponent } from './layout/header/header.component';
import { FooterComponent } from './layout/footer/footer.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { LoaderService } from './core/services/loader.service';
import { ToastService } from './core/services/toast.service';
import { RouteStateService } from './core/services/route-state.service';
import { SessionService } from './core/services/session.service';
import { HeaderBreadcrumbComponent } from './layout/header-breadcrumb/header-breadcrumb.component';
import { UserIdleModule } from 'angular-user-idle';
import { ThemeService } from './core/services/theme.service';
import { ApplicationStateService } from './core/services/application-state.service';
import { UserDataService } from './core/services/user-data.service';
import { MenuDataService } from './core/services/menu-data.service';

import { UserService } from './core/services/user.service';

import { httpInterceptorProviders } from './core/interceptor/auth-interceptor';

import { ListuserComponent } from './view/user/listuser/listuser.component';
import { ListpatientComponent } from './view/patient/listpatient/listpatient.component';



@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    LayoutComponent,
    MenuComponent,
    HeaderComponent,
    FooterComponent,
    DashboardComponent,
    HeaderBreadcrumbComponent,
    ListuserComponent,
    ListpatientComponent

  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    NgPrimeModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
          provide: TranslateLoader,
          useFactory: HttpLoaderFactory,
          deps: [HttpClient]
      }
    })
  ],
  providers: [
    httpInterceptorProviders,
    MessageService,
    AuthGuard,
    LoaderService,
    ToastService,
    RouteStateService,
    SessionService,
    ThemeService,
    ApplicationStateService,
    UserDataService,
    MenuDataService,
    UserService,

  ],
  bootstrap: [
    AppComponent
  ],
  entryComponents: [

  ]
})
export class AppModule { }

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}
