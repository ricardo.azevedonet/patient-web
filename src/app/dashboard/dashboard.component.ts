import { Component, OnInit } from '@angular/core';
import { MytranslatorService } from '../core/services/mytranslator.service';
import { DashboardService } from '../core/services/dashboard.service';
import { Dashboard } from '../core/models/dashboard.model';
import { MessageService } from 'primeng/api';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  countUsers: number;

  countPatients: number;

  barChartData: any;

  doughnutChartData: any;

  msgs: any[];

  dashboard: Dashboard;

  _mytranslatorService: MytranslatorService;

  constructor(private mytranslatorService: MytranslatorService, private dashboardService: DashboardService,
    private messageService: MessageService) {
    this._mytranslatorService = mytranslatorService;
    this.startService();
  }

  startService(){

     this.dashboardService.searchDashboard()
      .subscribe((result: any) => {
        this.dashboard = result;
      });

    setTimeout(() => {
      this.constructorDashboard();
    }, 1000);

  }

  constructorDashboard() {
    
    const LABEL_PATIENT = this.getTranslator('bundle.label.patient');
    const LABEL_USER = this.getTranslator('bundle.label.user');
    const LABEL_ACTIVE = this.getTranslator('bundle.label.active');
    const LABEL_INACTIVE = this.getTranslator('bundle.label.inactive');
    const WELCOME = this.getTranslator('bundle.label.welcome');

    this.countUsers = this.dashboard.countUsers;
    this.countPatients = this.dashboard.countPatients;
    const USER_COUNT_ACTIVE = this.dashboard.usersCountActive;
    const USER_COUNT_INACTIVE = this.dashboard.usersCountInaActive;
    const PATIENT_COUNT_ACTIVE = this.dashboard.patientsCountActive;
    const PATIENT_COUNT_INACTIVE = this.dashboard.patientsCountInaActive;
    console.log("WELCOME          v " + WELCOME);
    this.barChartData = {
      labels: [LABEL_USER, LABEL_PATIENT],
      datasets: [
        {
          label: LABEL_ACTIVE,
          backgroundColor: '#42A5F5',
          borderColor: '#1E88E5',
          data: [USER_COUNT_ACTIVE, PATIENT_COUNT_ACTIVE]
        },
        {
          label: LABEL_INACTIVE,
          backgroundColor: '#9CCC65',
          borderColor: '#7CB342',
          data: [USER_COUNT_INACTIVE , PATIENT_COUNT_INACTIVE ]
        }
      ]
    }

    this.doughnutChartData = {
      labels: [LABEL_ACTIVE, LABEL_INACTIVE],
      datasets: [
        {
          data: [this.dashboard.sumActive, this.dashboard.sumInaActive],
          backgroundColor: [
            "#FF6384",
            "#36A2EB",
            "#FFCE56"
          ],
          hoverBackgroundColor: [
            "#FF6384",
            "#36A2EB",
            "#FFCE56"
          ]
        }]
    };

    this.messageService.clear();
    this.messageService.add({severity: 'success', summary: '',  detail: WELCOME});
  }

  getTranslator(value) {
    return this._mytranslatorService.getTranslator(value)
  }

  ngOnInit() {
    this.constructorDashboard();
  }

}
