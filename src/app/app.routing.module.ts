import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AuthGuard } from './core/gaurds/auth.gaurd';
import { LoginComponent } from './login/login.component';
import { LayoutComponent } from './layout/layout.component';
import { DashboardComponent } from './dashboard/dashboard.component';

import { ListuserComponent } from './view/user/listuser/listuser.component';
import { ListpatientComponent } from './view/patient/listpatient/listpatient.component';

const appRoutes: Routes = [
    { path: 'login', component: LoginComponent, },
    {
        path: 'home', component: LayoutComponent, canActivate: [AuthGuard],
        children: [
            { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
            { path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuard] },
            { path: 'list-patient', component: ListpatientComponent, canActivate: [AuthGuard], data: { roles: ['USER'] } },
            { path: 'list-user', component: ListuserComponent, canActivate: [AuthGuard], data: { roles: ['ADMIN'] }  }
        ]
    },
    { path: '', redirectTo: 'login', pathMatch: 'full' }
];

@NgModule({
    imports: [RouterModule.forRoot(appRoutes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
