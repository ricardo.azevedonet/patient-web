import { Component, OnInit } from '@angular/core';
import { UserDataService } from '../core/services/user-data.service';
import { User } from '../core/models/user.model';
import { ToastService } from '../core/services/toast.service';
import { RouteStateService } from '../core/services/route-state.service';
import { SessionService } from '../core/services/session.service';

import { Observable } from 'rxjs';
import { UserService } from '../core/services/user.service';
import { AuthLoginInfo } from '../core/models/login-info';
import { Role } from '../core/models/role.model';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {

  username: string;

  password: string;

  private user: User;

    private _translate: TranslateService

  constructor(
    private userService: UserDataService,
    private toastService: ToastService,
    private routeStateService: RouteStateService,
    private sessionService: SessionService,
    private backEnd: UserService,
     private t: TranslateService
  ) {
     this._translate = t;
   }

  ngOnInit() {
    this.username = '';
    this.password = '';
  }

  onClickLogin() {
    this.user = new User();
    this.backEnd.attemptAuth(new AuthLoginInfo(this.username, this.password)).subscribe(
        data => {
            this.user.id = Number(data.id);
            this.user.token = data.token;
            this.user.username = data.username;
            this.user.email = data.email;
            this.user.name = data.username;

            data.roles.forEach(r => {
              this.user.role.push(r);
            });

            this.sessionService.saveToken(data.token);
            this.sessionService.setItem('currentUser', this.user);
            this.routeStateService.add('Home', '/home', null, true);
            return;
        },
        error => {
          this.toastService.addSingle('error', '', 'Invalid user.');
          return;
        }
    );
  }

   get translate(){
    return this._translate;
  }

}
