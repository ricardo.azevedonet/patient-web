import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { ToastService } from '../../../core/services/toast.service';
import { HttpClient } from '@angular/common/http';
import { MytranslatorService } from '../../../core/services/mytranslator.service';
import { ConfirmationService, LazyLoadEvent } from 'primeng/api';
import { PatientService } from '../../../core/services/patient.service';
import { Patient } from '../../../core/models/patient.model';
import { DialogService } from 'primeng/dynamicdialog';

import { GenericFilter } from '../../../core/models/generic/generic-filter.model';
import { SessionService } from '../../../core/services/session.service';
import { AddresszipcodeService } from '../../../core/services/addresszipcode.service';
import { birthDateValidator } from '../../../core/validators/birthdate.validators';
import { DatePipe } from '@angular/common';
import { CpfValidator } from '../../../core/validators/cpf.validators';

@Component({
  selector: 'app-listpatient',
  templateUrl: './listpatient.component.html',
  styleUrls: ['./listpatient.component.css'],
  providers: [DialogService, DatePipe]
})
export class ListpatientComponent implements OnInit {

  public listForm: FormGroup;

  public patients: Patient[];

  public patientList: Patient[] = [];

  patientSelected: Patient;

  private eventfirst: number;

  private eventRows: number;

  public patientForm: FormGroup;

  display: boolean;

  header: string;

  totalRecords: any;

  cols: { field: string; header: string; }[];

  loading: boolean = false;

  constructor(private fb: FormBuilder, private toastService: ToastService,
    http: HttpClient, private patientService: PatientService, private dialogService: DialogService,
    private mytranslatorService: MytranslatorService, private confirmationService: ConfirmationService,
    private sessionService: SessionService, private addresszipcodeService: AddresszipcodeService,
    private datePipe: DatePipe,
    ) {
  }

  ngOnInit(): void {
    this.createForm();
    this.createColsTable();
    this.createFormUser();
  }

  createForm() {
    this.listForm = this.fb.group({
      patients: Patient,
      name: [],
      status: [],
    });
  }

  createColsTable() {
    this.cols = [
      { field: 'name', header: 'bundle.label.name' },
      { field: 'cpf', header: 'bundle.label.cpf' },
      { field: 'email', header: 'bundle.label.email' }
    ];
  }

  createFormUser() {
    this.patientForm = this.fb.group({
      id: new FormControl(''),
      name: new FormControl('', Validators.required),
      cpf: new FormControl('', [Validators.required, CpfValidator.isValidf]),
      email: new FormControl('', [Validators.required, Validators.email]),
      dataBirth: new FormControl('', [Validators.required, birthDateValidator]),
      telefone: new FormControl('', Validators.required),
      zipCode: new FormControl('', Validators.required),
      address: new FormControl('', Validators.required),
      number: new FormControl('', Validators.required),
      complement: new FormControl('', []),
      district: new FormControl('', Validators.required),
      city: new FormControl('', Validators.required),
      state: new FormControl('', Validators.required),
    });
  }




  autoCompleteSingle(event) {
    this.patientService.autoComplete(event.query)
      .subscribe((result: any) => {

        this.patients = [];
        result.forEach(p => {
          this.patients = [...this.patients, p];
        });
      });
  }

  onClickSearch() {
    this.eventfirst = 0;
    this.eventRows = 5;
    this.processFindLazy(this.getFilter(), this.eventfirst, this.eventRows);
  }

  onRowEditInit(patientSelected: Patient) {
    this.patientSelected = patientSelected == null ? patientSelected = new Patient() : patientSelected;

    this.patientForm.get('id').setValue(patientSelected.id);
    this.patientForm.get('name').setValue(patientSelected.name);
    this.patientForm.get('cpf').setValue(patientSelected.cpf);
    this.patientForm.get('email').setValue(patientSelected.email);
    this.patientForm.get('dataBirth').setValue(this.datePipe.transform(patientSelected.dataBirth, 'dd/MM/yyyy'));
    this.patientForm.get('telefone').setValue(patientSelected.telefone);
    this.patientForm.get('zipCode').setValue(patientSelected.zipCode);
    this.patientForm.get('address').setValue(patientSelected.address);
    this.patientForm.get('number').setValue(patientSelected.number);
    this.patientForm.get('complement').setValue(patientSelected.complement);
    this.patientForm.get('district').setValue(patientSelected.district);
    this.patientForm.get('city').setValue(patientSelected.city);
    this.patientForm.get('state').setValue(patientSelected.state);

    this.display = true;
    this.header = (patientSelected != null && patientSelected.id != null ? 'bundle.label.update' : 'bundle.label.create');
  }

  cancel(event) {
    this.display = false;
  }

  getFilter() {
    const filter = new GenericFilter();

    const name = this.listForm.get('name').value;
    const status = this.listForm.get('status').value;

    filter.name = name === '' ? null : name;
    filter.status = status === 'Inactive' ? 'I' : (status === 'Active') ? 'A' : null;

    return filter;
  }

  processFindLazy(filter: GenericFilter, page: number, size: number) {
    this.patientService.findPaginator(filter, page, size)
      .subscribe((result: any) => {
        this.patientList = [];

        if (result.content.length > 0) {
          result.content.forEach(patient => {
            this.patientList.push(patient);
          });
        }

        this.totalRecords = result.totalElements;
      });
  }

  onPageChanged(event: LazyLoadEvent) {
    this.eventfirst = event.first;
    this.eventRows = event.rows;

    setTimeout(() => {
      if (this.patientList) {
        this.processFindLazy(this.getFilter(), event.first / event.rows, event.rows);
      }
    }, 1000);
  }

  confirmChangeStatus(patientSelected: Patient) {
    const parametres = JSON.parse('{"value1":"' + patientSelected.name + '"}');
    this.confirmationService.confirm({
      message: this.mytranslatorService.getTranslatorarametres('bundle.message.confirmation.change.status.patient', parametres),
      header: this.mytranslatorService.getTranslator('bundle.label.confirmation'),
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        patientSelected.userChange = this.sessionService.getItem('currentUser');
        this.patientService.changeStatus(patientSelected)
          .subscribe(data => {
            const msg =
              this.mytranslatorService.getTranslator('bundle.label.patient') + ' '
              + this.mytranslatorService.getTranslator('bundle.label.change') + ' '
              + this.mytranslatorService.getTranslator('bundle.label.with') + ' '
              + this.mytranslatorService.getTranslator('bundle.label.success');
            this.toastService.addSingle(this.mytranslatorService.getTranslator('bundle.label.success'), '', msg);
            this.processFindLazy(this.getFilter(), this.eventfirst / this.eventRows, this.eventRows);
            this.onClickSearch();
          },
          error => {
            console.log('changeStatus' + error);
          });
      },
      reject: () => {
        console.log('reject');
      }
    });

  }

  save() {

    const patient = new Patient();

    patient.id = this.patientForm.get('id').value;
    patient.name = this.patientForm.get('name').value;
    patient.cpf = this.patientForm.get('cpf').value;
    patient.email = this.patientForm.get('email').value;
    patient.dataBirth = new Date(this.patientForm.get('dataBirth').value);
    patient.telefone = this.patientForm.get('telefone').value;
    patient.zipCode = this.patientForm.get('zipCode').value;
    patient.address = this.patientForm.get('address').value;
    patient.number = this.patientForm.get('number').value;
    patient.complement = this.patientForm.get('complement').value;
    patient.district = this.patientForm.get('district').value;
    patient.city = this.patientForm.get('city').value;
    patient.state = this.patientForm.get('state').value;
    patient.userChange = this.sessionService.getItem('currentUser');

    this.patientService.addOrUpdate(patient).subscribe(
      data => {
        let msg = this.mytranslatorService.getTranslator('bundle.label.patient') + ' '
          + this.mytranslatorService.getTranslator('bundle.label.saved') + ' '
          + this.mytranslatorService.getTranslator('bundle.label.with') + ' '
          + this.mytranslatorService.getTranslator('bundle.label.success');
        this.toastService.addSingle(this.mytranslatorService.getTranslator('bundle.label.success'), '', msg);
        this.display = false;
        this.onClickSearch();
      },
      error => {
        console.log('error save := JSON ' + JSON.stringify(error));
        console.log(error);
      }
    );

  }


  findZipCode(event) {

    const zipCode = this.patientForm.controls["zipCode"].value;

    this.addresszipcodeService.findZipcode(zipCode)
      .subscribe((result: any) => {
        this.patientForm.get('zipCode').setValue(result.cep);
        this.patientForm.get('address').setValue(result.logradouro);
        this.patientForm.get('district').setValue(result.bairro);
        this.patientForm.get('city').setValue(result.localidade);
        this.patientForm.get('state').setValue(result.uf);
      });
  }

}
