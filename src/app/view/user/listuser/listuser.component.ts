import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { ConfirmationService, LazyLoadEvent } from 'primeng/api';


import { ToastService } from '../../../core/services/toast.service';
import { UserDataService } from '../../../core/services/user-data.service';
import { UserAutocomplete } from '../../../core/models/userautocomplete.model';
import { GenericFilter } from '../../../core/models/generic/generic-filter.model';
import { User } from '../../../core/models/user.model';

import { MytranslatorService } from '../../../core/services/mytranslator.service';
import { DialogService } from 'primeng/dynamicdialog';

@Component({
  selector: 'app-listuser',
  templateUrl: './listuser.component.html',
  styleUrls: ['./listuser.component.css'],
  providers: [DialogService]
})
export class ListuserComponent implements OnInit {

  public listUserform: FormGroup;

  public users: UserAutocomplete[];

  public usersList: User[] = [];

  public totalRecords: number;

  public cols: any[];

  display: boolean = false;
  verifyChangeStatus: boolean = false;

  header: string;

  public userSelected: User;

  userform: FormGroup;

  loading: boolean = false;

  private eventfirst: number;
  private eventRows: number;

  constructor(private fb: FormBuilder, private toastService: ToastService,
    http: HttpClient, private userDataService: UserDataService, private dialogService: DialogService,
    private mytranslatorService: MytranslatorService, private confirmationService: ConfirmationService) { }

  ngOnInit() {
    this.createForm();

    this.cols = [
      { field: 'name', header: 'bundle.label.name' },
      { field: 'username', header: 'bundle.label.username' },
      { field: 'email', header: 'bundle.label.email' },
      { field: 'userChangeName', header: 'bundle.label.user_change' }
    ];
    this.createFormUser();
  }

  createForm() {
    this.listUserform = this.fb.group({
      users: UserAutocomplete,
      status: String,
    });
  }

  createFormUser() {
    this.userform = this.fb.group({
      'id': new FormControl(''),
      'name': new FormControl('', Validators.required),
      'username': new FormControl('', Validators.required),
      'email': new FormControl('', [Validators.required, Validators.email])
    });
  }

  onClickSearch() {
    this.eventfirst = 0;
    this.eventRows = 100;
    this.processFindLazy(this.getFilter(), this.eventfirst, this.eventRows);
  }

  filterSPUserSingle(event) {
    this.filterSPUsers(event.query, 'single');
  }

  filterSPUsers(query, type) {

    this.userDataService.autoCompleteNameUser(query)
      .subscribe((result: any) => {
        if (type === 'single') {
          this.users = [];
          const allUsers = [];

          result.forEach(u => {
            const user = new UserAutocomplete();
            user.id = Number(u.id);
            user.username = u.username;
            user.email = u.email;
            allUsers.push(user);
          });

          allUsers.forEach(user => {
            this.users = [...this.users, user];
          });
        }
      });
  }

  onPageChanged(event: LazyLoadEvent) {
    this.eventfirst = event.first;
    this.eventRows = event.rows;

    setTimeout(() => {
      if (this.usersList) {
        this.processFindLazy(this.getFilter(), event.first / event.rows, event.rows);
      }
    }, 1000);
  }

  processFindLazy(filter: GenericFilter, page: number, size: number) {
    this.userDataService.findUserPaginator(filter, page, size)
      .subscribe((result: any) => {
        this.usersList = [];
        result.content.forEach(u => {
          const user = new User();
          user.id = u.id;
          user.name = u.name;
          user.email = u.email;
          user.username = u.username;
          user.status = u.status;
          user.userChangeName = u.userChangeName;
          user.dateChange = u.dateChange;
          this.usersList.push(user);
        });
        this.totalRecords = result.totalElements;
      });
  }

  getFilter() {
    const filter = new GenericFilter();

    filter.id = this.listUserform.get('users').value.id === null ? 1 : this.listUserform.get('users').value.id;
    filter.name = this.listUserform.get('users').value.username === '' ? 'xxx' : this.listUserform.get('users').value.username;
    filter.status = this.listUserform.get('status').value === 'Inactive' ? 'I' :
      (this.listUserform.get('status').value === 'Active') ? 'A' : null;
    return filter;
  }

  onRowEditInit(userSelected: User) {
    this.userSelected = userSelected == null ? userSelected = new User() : userSelected;

    this.userform.get('id').setValue(userSelected.id);
    this.userform.get('name').setValue(userSelected.name);
    this.userform.get('username').setValue(userSelected.username);
    this.userform.get('email').setValue(userSelected.email);

    this.display = true;
    this.header = (userSelected != null && userSelected.id != null ? 'bundle.label.update' : 'bundle.label.create');
  }

  cancel(event) {
    this.display = false;
  }

  confirmChangeStatus(userSelected: User) {
    const parametres = JSON.parse('{"value1":"' + userSelected.username + '"}');

    this.confirmationService.confirm({
      message: this.mytranslatorService.getTranslatorarametres('bundle.message.confirmation.change.status.user', parametres),
      header: this.mytranslatorService.getTranslator('bundle.label.confirmation'),
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.userDataService.changeStatusUser(userSelected)
          .subscribe(data => {
            const msg =
              this.mytranslatorService.getTranslator('bundle.label.user') + ' '
              + this.mytranslatorService.getTranslator('bundle.label.change') + ' '
              + this.mytranslatorService.getTranslator('bundle.label.with') + ' '
              + this.mytranslatorService.getTranslator('bundle.label.success');
            this.toastService.addSingle(this.mytranslatorService.getTranslator('bundle.label.success'), '', msg);
            this.processFindLazy(this.getFilter(), this.eventfirst / this.eventRows, this.eventRows);
            this.onClickSearch();
          },
            error => {
              console.log('error changeStatusUser = ' + error);
              console.log(error);
            });
      },
      reject: () => {
        console.log('reject');
      }
    });
  }

  save() {
    this.userDataService.addOrUpdateUser(this.userform.controls['id'].value,
      this.userform.controls["name"].value,
      this.userform.controls["username"].value,
      this.userform.controls['email'].value,
      this.userSelected).subscribe(
        data => {
          let msg = this.mytranslatorService.getTranslator('bundle.label.user') + ' '
            + this.mytranslatorService.getTranslator('bundle.label.saved') + ' '
            + this.mytranslatorService.getTranslator('bundle.label.with') + ' '
            + this.mytranslatorService.getTranslator('bundle.label.success');
          this.toastService.addSingle(this.mytranslatorService.getTranslator('bundle.label.success'), '', msg);
          this.display = false;
          this.onClickSearch();
        },
        error => {
          console.log('error addOrUpdateUser := ' + error);
          console.log(error);
        }
      );
  }


}
